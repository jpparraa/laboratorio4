/*=========================================================================
  ENCABEZADO DE LA CLASE 
  =========================================================================*/
/**
 * CLASE: LlaveInvalidaException.java
 * <br>OBJETIVO: Clase que define la excepci� de la llave no se encuentra en el VideoCentro.
 * <br>ASIGNATURA: java basico
 * @version 1.0 24/07/2005
 * @author William Mendoza Rodriguez;
 * @author DIANA C FARIETA G
 */

public class LlaveInvalidaException extends Exception {


    /*=========================================================================
      METODOS CONSTRUCTORES
      =========================================================================*/
    /** 
     * Inicializa una instancia "vacia" de un nuevo objeto <B>PrecioInvalidoException</B>
     */
   LlaveInvalidaException () {}

    /*=========================================================================
      DEFINICION E IMPLEMENTACION DE LOS METODOS ANALIZADORES Y MODIFICADORES
      =========================================================================*/
    /** 
     * Retorna la representacion de la Excepcion. El valor es retornado en un objeto String.
     * <br>Precondicion: El objeto debe existir.
     * @return El mensaje: "Precio Invalido. Necesita ser mayor que cero"
     */
   public String toString() {
      return "Llave Invalida.  La llave digitada no se encuentra registrada.";
   }
}
