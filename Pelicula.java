//package VideoCentro;

/**
 * Clase que contiene información de una película.
 *
 * @author Juan Parra
 *
 * @version 0.1
 *
 */

public class Pelicula{
	
	//Atributos
	protected String id;
	protected String nombre;
	protected int agno;
	protected double duracion;
	protected double costoAlquiler;

	//Métodos constructores
	
	/**
	 *
	 * Crea un objeto de tipo pelicula.
	 *
	 */

	public Pelicula(){
		this.id = "";
		this.nombre = "";
		this.agno = 0;
		this.duracion = 0;
		this.costoAlquiler = 0;
	
	}
	/**
	 *
	 * Crea un objeto de tipo Película.
	 *
	 * @param id Id de la película.
	 * @param nombre Nombre de la película.
	 * @param agno Año de la película,
	 * @param duracion Duración de la película.
	 * @param costoAlquiler Costo del alquiler.
	 *
	 */

	public Pelicula(String id, String nombre, int agno, double duracion, double costoAlquiler){
		this.id = id;
		this.nombre = nombre;
		this.agno = agno;
		this.duracion = duracion;
		this.costoAlquiler = costoAlquiler;
	}

	//Métodos Analizadores
	
	/**
	 *
	 * Retorna el ID de la película.
	 *
	 * @return ID de la película.
	 *
	 */

	public String getId(){
		return this.id;
	}
	
	/**
	 *
	 * Retorna el nombre de la película.
	 *
	 * @return Nombre de la película.
	 *
	 */

	public String getNombre(){
		return this.nombre;
	}

	/**
	 *
	 * Retorna el año de la película.
	 *
	 * @return Año de la película.
	 *
	 */

	public int getAgno(){
		return this.agno;
	}

	/**
	 *
	 * Retorna la duración en minutos de la película.
	 *
	 * @return Duración de la película.
	 *
	 */

	public double getDuracion(){
		return this.duracion;
	}
	
	/**
	 *
	 * Retorna la duración en horas y minutos de la película.
	 *
	 * @return Duración en horas y minutos de la película.
	 *
	 */

	public String getDuracionHoras(){
		return  (int)this.getDuracion() / 60 + " horas "+
			(int)this.getDuracion() % 60 + " minutos";
	}

	/**
	 *
	 * Retorna el costo del alquiler.
	 *
	 * @return Costo del alquiler.
	 *
	 */
	public double getCostoAlquiler(){
		return this.costoAlquiler;
	}

	//Métodos Modificadores
	
	/**
	 *
	 * Modifica el id de la película.
	 *
	 * @param id Nuevo id del Objeto.
	 *
	 */

	public void setId(String id){
		this.id = id;
	}

	/**
	 *
	 * Modifica el nombre de la película.
	 *
	 * @param nombre Nuevo nombre de la película.
	 *
	 */

	public void setNombre(String nombre){
		this.nombre = nombre;
	}

	/**
	 *
	 * Modifica el año de la película.
	 *
	 * @param agno Nuevo año de la película.
	 *
	 */

	public void setAgno(int agno){
		this.agno = agno;		
	}

	/**
	 *
	 * Modifica la duración de la película.
	 *
	 * @param duracion Nueva duración de la película.
	 *
	 */

	public void setDuracion(double duracion){
		this.duracion = duracion;
	}

	/**
	 *
	 * Modifica el costo del alquiler.
	 *
	 * @param costoAlquiler Nuevo costo del alquiler de la película.
	 *
	 */

	public void setCostoAlquiler(double costoAlquiler){
		this.costoAlquiler = costoAlquiler;
	}

	/**
	 *
	 * Retorna los datos de la película.
	 *
	 * @return ID, Nombre, Año, Duración y Costo de la película.
	 *
	 */

	public String toString(){
		return  "ID            : " +this.getId()+ "\n"+
			"Nombre        : " +this.getNombre()+ "\n"+
			"Año           : " +this.getAgno()+ "\n"+
			"Duración      : " +this.getDuracion()+ "\n"+
			"Costo alquiler: " +this.getCostoAlquiler()+ "\n";
	}

	/**
	 *
	 * Retorna Nombre, duración y costo de la película.
	 *
	 * @return Nombre, duración en horas y minutos y costo de la película.
	 *
	 */

	public String datosPelicula(){
		return  "Nombre        : " +this.getNombre()+ "\n"+
			"Duración      : " +this.getDuracionHoras()+ "\n"+
			"Costo alquiler: " +this.getCostoAlquiler()+ "\n";
	}
}
