/*=========================================================================
  ENCABEZADO DE LA CLASE 
  =========================================================================*/

import java.util.*;
//import java.lang.Boolean.*;



/**
 * CLASE: VideoCentro.java
 * <br>OBJETIVO: Clase que describe un VideoCentro.
 * <br>ASIGNATURA: java
 * @version 1.0 24/07/2005
 * @author  William Mendoza Rodrguuez.
 * @author Diana c farietta g
 */

public class VideoCentro {
    
    // Atributos privados de la clase VideoCentro.
    //private String nombre;
    private Hashtable videos;
    
   /*=========================================================================
      METODOS CONSTRUCTORES
      =========================================================================*/
    /** 
     * Inicializa una instancia "vacia" de un nuevo objeto <B>PrecioInvalidoException</B>
     */
    
	VideoCentro() {
		videos = new Hashtable();
	}
    
  
    /*=========================================================================
      DEFINICION E IMPLEMENTACION DE LOS METODOS ANALIZADORES Y MODIFICADORES
      =========================================================================*/


	/** 
     * Metodo Modificador. Registra la pelicula en el Video Centro.
     *
     * @param p es la pelicula a registrar.
     */

	public void registrar(Pelicula p) {
		videos.put(p.getId(), p);
	}
	
	/**
	 * Retorna True si se logra eliminar una pelicula de la libreria, false en caso contrario
	 *  Elobejto retornado es de tipo boolean.
	 * <br>Precondicion: True. El obejto debe existir.  	
	 * @return Si se logro o no eliminar el objeto de la libreria de peliculas.
	 */
  
	public boolean eliminar(String llave) {
		Object resultado = null;
		
		resultado = videos.remove(llave);
		if (resultado == null)
			return false;
		else
			return true;
	}
	
	/**
	 * Retorna True si la llave existe en el Hashtable, false en caso contrario
	 *  Elobejto retornado es de tipo boolean.
	 * <br>Precondicion: True.   	
	 * @return Si la llave existe o no en el Hashtable.
	 */
	 
	public boolean existeLlave(String llave){
		boolean resp; 
		
		resp = videos.containsKey(llave);
		
		return resp;
	
	}
	
	
	/**
	 * Retorna Los datos de la pelicula dada su llave.
	 * El objeto retornado es de tipo String.
	 * <br>Precondicion: True. El obejto debe existir.  	
	 * @return Los datos de la pelicula de la llave dada.
	 */

	public String getInfo(String llave){
		
		Pelicula p;
		
		p = (Pelicula)videos.get(llave);	
		return p.toString();
		
	}

/**
	 * Retorna Los datos de todas las peliculas del Video Centro.
	 * El objeto retornado es de tipo String.
	 * <br>Precondicion: True. 	
	 * @return Los datos de todas las peliculas existentes en el video.
	 */


	public String toString() {
		String s = "";
		Pelicula p;
		
		int NumElem = 0;

		//NumElem = videos.size();
		
		for(Enumeration e = videos.elements(); e.hasMoreElements();){
			p = (Pelicula)e.nextElement();
			s+= p.datosPelicula();
		}
 		return s;
	}

}
