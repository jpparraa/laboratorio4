/*=========================================================================
  ENCABEZADO DE LA CLASE 
  =========================================================================*/
/**
 * CLASE: PrecioInvalidoException.java
 * <br>OBJETIVO: Clase que define la excepci� de que el precio no es mayor que cero.
 * <br>ASIGNATURA: JAVA BASICO
 * @version 1.0 24/07/2005
 * @author William Mendoza Rodriguez;
 * @author DIANA C FARIETTA G
 */

public class PrecioInvalidoException extends Exception {


    /*=========================================================================
      METODOS CONSTRUCTORES
      =========================================================================*/
    /** 
     * Inicializa una instancia "vacia" de un nuevo objeto <B>PrecioInvalidoException</B>
     */
   PrecioInvalidoException () {}

    /*=========================================================================
      DEFINICION E IMPLEMENTACION DE LOS METODOS ANALIZADORES Y MODIFICADORES
      =========================================================================*/
    /** 
     * Retorna la representacion de la Excepcion. El valor es retornado en un objeto String.
     * <br>Precondicion: El objeto debe existir.
     * @return El mensaje: "Precio Invalido. Necesita ser mayor que cero"
     */
   public String toString() {
      return "Precio Invalido. Necesita ser mayor que cero";
   }
}
