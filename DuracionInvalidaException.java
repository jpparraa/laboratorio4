/*=========================================================================
  ENCABEZADO DE LA CLASE 
  =========================================================================*/
/**
 * CLASE: DuracionInvalidaException.java
 * <br>OBJETIVO: Clase que define la excepci� de que la duraci� no es mayor que cero.
 * <br>ASIGNATURA: java basico
 * @version 1.0 24/07/2005
 * @author William Mendoza Rodriguez;
 * @author DIANA C FARIETTA G
 */

public class DuracionInvalidaException extends Exception {


    /*=========================================================================
      METODOS CONSTRUCTORES
      =========================================================================*/
    /** 
     * Inicializa una instancia "vacia" de un nuevo objeto <B>PrecioInvalidoException</B>
     */
   DuracionInvalidaException () {}

    /*=========================================================================
      DEFINICION E IMPLEMENTACION DE LOS METODOS ANALIZADORES Y MODIFICADORES
      =========================================================================*/
    /** 
     * Retorna la representacion de la Excepcion. El valor es retornado en un objeto String.
     * <br>Precondicion: El objeto debe existir.
     * @return El mensaje: "Duracion Invalida. Necesita ser mayor que cero"
     */
   public String toString() {
      return "Duracion Invalida. Necesita ser mayor que cero";
   }
}
