/*=========================================================================
  ENCABEZADO DE LA CLASE 
  =========================================================================*/
/**
 * CLASE: AnioInvalidoException.java
 * <br>OBJETIVO: Clase que define la excepci� de que el a� no sea erroneo.
 * <br>ASIGNATURA: java basico
 * @version 1.0 24/07/2005
 * @author William Mendoza Rodriguez;
 * @author DIANA C FARIETTA G;
 */

public class AnioInvalidoException extends Exception {


    /*=========================================================================
      METODOS CONSTRUCTORES
      =========================================================================*/
    /** 
     * Inicializa una instancia "vacia" de un nuevo objeto <B>A�InvalidoException</B>
     */
   AnioInvalidoException () {}

    /*=========================================================================
      DEFINICION E IMPLEMENTACION DE LOS METODOS ANALIZADORES Y MODIFICADORES
      =========================================================================*/
    /** 
     * Retorna la representacion de la Excepcion. El valor es retornado en un objeto String.
     * <br>Precondicion: El objeto debe existir.
     * @return El mensaje: "A� invalido. Necesita pertenecer a (1940<2005).
     */
   public String toString() {
      return "Anio Invalido. Necesita pertenecer a (1900<2020)";
   }
}
